<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_once __DIR__ . "/../models/Activite.php";

class creerActivite
{
    private static function getActivites(): array
    {
        // Créer une nouveau tableau qui contiendra les activités s'il n'existe pas
        if (!isset($_SESSION["activites"])) {
            $_SESSION["activites"] = [];
        }
        return $_SESSION["activites"];
    }

    /*TODO Compléter la méthode de validation
    Critères de validation :
    - Accepter lettres, ",", ";" "-" "'" pour les champs texte */
    private static function estValide(string $chaine): bool
    {
        if(preg_match("[A-Za-z-,-,;-;'-', ]", $chaine) == 1)
        {
            return true;
        }
        else
        {
            return false;
        }

    }


    public static function ajoutNouvelleActivite(array $details): bool
    {
        $creation = false;
        //@TODO Ajouter les instructions pour valider les données de l'activité ($details) à l'aide de la méthode estValide
        if(self::estValide() == true)
        {
            //Si valide, créer l'activité
            $activite = new Activite($details);

            //@TODO Si valide, l'ajouter au tableau des activités
            array_push(self::getActivites(), $activite);

            //Retourner vrai si l'activité à été ajoutée
            $creation = true;
        }


        //Retourner vrai si l'activité a été ajoutée, faux sinon
        return $creation;
    }
}