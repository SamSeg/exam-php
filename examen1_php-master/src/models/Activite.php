<?php


class Activite
{
    private string $lieu;
    private DateTime $date;
    private string $activite;
    private int $intensite;
    private int $duree;

    public function __construct(array $detailsActivite)
    {
        foreach ($detailsActivite as $detail => $valeur)
        {
            if(property_exists($this, $detail))
            {
                $this->$detail = $valeur;
            }
        }
    }

    public function getLieu(): string
    {
        return $this->lieu;
    }
    public function setLieu(string $lieu): void
    {
        $this->lieu = $lieu;
    }


    public function getActivite(): string
    {
        return $this->activite;
    }
    public function setActivite(string $activite): void
    {
        $this->activite = $activite;
    }


    public function getIntensite(): int
    {
        return $this->intensite;
    }
    public function setIntensite(int $intensite): void
    {
        $this->intensite = $intensite;
    }


    public function getDuree(): int
    {
        return $this->duree;
    }
    public function setDuree(int $duree): void
    {
        $this->duree = $duree;
    }
}